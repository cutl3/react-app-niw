import React, {  } from "react";
import './App.scss';
import { Provider } from 'react-redux';
import { myReducer } from './redux/reducer';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import Routes from './Routers';
import 'antd/dist/antd.css';

export const store = createStore(myReducer, applyMiddleware(thunk));

function App() {
  return (
    <div className="App">
    <Provider store={store}>
      <Routes />
    </Provider>
  </div>
  );
}

export default App;