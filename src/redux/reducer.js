import CONSTANTS from "../utils/helpers/constant"

const initialState = {
    ads: {},
};

export const myReducer = (state, action) => {
    if (typeof state === 'undefined') {
        return initialState;
    }
    switch (action.type) { 
        case CONSTANTS.SET_ADS:
            return { ...state, ads: state.dataAds};
        default:
            return state;
    }
};
