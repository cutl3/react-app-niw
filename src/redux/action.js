import CONSTANTS from "../utils/helpers/constant";
import API from "../utils/api"

export function setAds(dataAds) {
    return {
        type: CONSTANTS.SET_ADS,
        dataAds
    }
}
