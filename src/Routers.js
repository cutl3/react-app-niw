import React, { Suspense, useEffect , lazy } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import Home from "./pages/Home";
import Header from "./components/Header";
import Footer from "./components/Footer";
const About =  lazy(() => import('./pages/About'))

const Routes = (props) => {
    return (
        <Router>
            <Header/>
            <div id="content">
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/about" component={WaitingComponent(About)} />
                </Switch>
            </div>
            <Footer/>
        </Router>
    )
}

export default Routes;

function WaitingComponent(Component) {
    return props => (
        <Suspense fallback={<div>Loading....</div>}>
            <Component {...props} />
        </Suspense>
    );
}


