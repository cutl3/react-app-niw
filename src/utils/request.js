import axios from "axios";
// const http = require("http");
// const https = require("https");
const baseDomain = process.env.REACT_APP_API_URL;
const baseURL = `${baseDomain}/`;

 const getInstanceAxios = (baseAPI, notAuthorization ) => {
     console.log(baseAPI,'baseAPI');
    const instance = axios.create({
        baseURL: baseAPI,
        // httpAgent: new http.Agent({ keepAlive: true }),
        // httpsAgent: new https.Agent({ keepAlive: true }),
    });

    instance.interceptors.request.use(
        function (config) {
            config.headers = {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: notAuthorization ? '' : "Bearer " + localStorage.getItem("accessToken"),
            };
            return config;
        },
        function (error) {
            return Promise.reject(error);
        }
    );

    instance.interceptors.response.use(
        function (response) {
            try {
                if (response.status !== 200) return Promise.reject(response.data);
                return response.data;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        function (error) {
            return Promise.reject(error);
        }
    );
    return instance;
}


export default getInstanceAxios(baseURL);

