import React, { useEffect , useState } from 'react';
import { Link } from 'react-router-dom';
import API from "../../utils/api";
import { DatePicker } from 'antd';
import {setAds } from "../../redux/action";
import { connect } from 'react-redux';
import './index.scss'

const Home = (props) => {
    const [date,setDate] = useState(null);
    useEffect(() => {
        API.apiTesting()
            .then((response) => {
                console.log(response, 'response');
                props.setAds(response.banner)
            })
            .catch((er) => {
                console.log(er);
            });
    }, []);
    return (
        <div className="home">
            <h2>Đây là home</h2>
            <Link to='/about'>About</Link> 
            {props.ads ? props.ads.title : '' }
            {/* <div style={{ width: 400, margin: '100px auto' }}>
                <DatePicker />
                <div style={{ marginTop: 16 }}>
                    Selected Date: {date ? date.format('YYYY-MM-DD') : 'None'}
                </div>
            </div> */}
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        ads: state.ads
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setAds: (value) => dispatch(setAds(value))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
